<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Just like slightly more detailed Sitemap of the whole website.
*/
/*----------------------------------------------------------------------------*/
/*
| Guest area
*/
Route::get('/'			, 'HomeController@index');
Route::get('/home'		, 'HomeController@index');
Route::get('/categories', 'HomeController@categories');
Route::get('/blog'		, 'HomeController@blog');
Route::get('/blog/post'	, 'HomeController@post');
Route::get('/contact'	, 'HomeController@contact');
Route::get('/search/1'	, function(){	return view('search-1');	});
Route::get('/search/2'	, function(){	return view('search-2');	});
Route::get('/how-it-works'	, function(){ return view('pros.how-it-works'); });
Route::get('/professionals'	, function(){ return view('pros.proregister'); });
Route::post('/professionals/post'	, 'ProController@registerprofessional');

/*----------------------------------------------------------------------------*/
/*
| Authentication
*/
Route::controllers([
	'auth' => 'Auth\AuthController',	
	'password' => 'Auth\PasswordController',
]);

/*----------------------------------------------------------------------------*/
/*
| Users: Auth mandatory area
*/
Route::get('/users'					, 'CustomersController@index');
Route::get('/users/applications'	, 'CustomersController@applications');
Route::get('/users/connections/'	, 'ApplicationsController@request');
Route::get('/users/messages'		, 'CustomersController@messages');
Route::post('/users/messages/send'		, 'CustomersController@send');
Route::get('/users/messages/view/{id}'		, 'CustomersController@messagesview');
	/*
	| Job area
	*/
	Route::get('/users/jobs'			, 'CustomersController@jobs');
	Route::get('/users/jobs/post'		, 'CustomersController@post');
	Route::post('/users/jobs/post'		, 'CustomersController@storenewpost');
	Route::get('/users/jobs/view/{id}'	, 'CustomersController@view');
/*----------------------------------------------------------------------------*/
/*
| Pros: Auth mandatory area
*/
Route::get('/pros'					, function(){ return Redirect::to('/pros/newsfeed');});
Route::get('/pros/newsfeed'			, 'ProController@newsfeed');
Route::get('/pros/messages'			, 'ProController@messages');
Route::post('/pros/messages/send'	, 'ProController@send');
Route::get('/pros/messages/view/{id}'			, 'ProController@messagesview');
Route::get('/pros/profile/view/{id}'			, 'ProController@profileview');
	/*
	| Job area
	*/
	Route::get('/pros/jobs'				, 'ProController@jobs');
	Route::get('/pros/jobs/view/{id}'	, 'ProController@view');
	Route::post('/pros/jobs/apply/'		, 'ProController@apply');

