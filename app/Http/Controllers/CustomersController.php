<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Auth;
use Redirect;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    public function __construct(){
        
        $this->middleware('auth');
    }


/*------------------------------------------------
*   validating customer and professional
*/
    public function index(){
        
        $is_check_user = \App\Professionals::where('customer_id', Auth::user()->id)->count();
        if($is_check_user == 1){
            // it's a pro
            return Redirect::to(URL::to('/')."/pros/newsfeed");
        }
        else{
                return Redirect::to(URL::to('/')."/users/jobs");
            }
            
        } 

/*------------------------------------------------
*   showing the list of applicants for the job
*/
    public function applications(){
            $id = Auth::user()->id;
            $job_applications = DB::select("SELECT applications.* FROM users JOIN applications ON users.id = applications.cust_id WHERE users.id = ?",[$id]);
            //return $job_applications;
            return view('users.job-applications')->with('data', $job_applications);        
    }

/*------------------------------------------------
*   accepting a connection
*/
    public function acceptconnection(){
            $id = Auth::user()->id;
            $job_applications = DB::select("SELECT applications.* FROM users JOIN applications ON users.id = applications.cust_id WHERE users.id = ?",[$id]);
            //return $job_applications;
            return view('users.job-applications')->with('data', $job_applications);        
    }
/*------------------------------------------------
*   showing open jobs posted by the customer
*/
    public function jobs(){
        $data = \App\Jobs::where('customer_id', Auth::user()->id)->get();
        return view('users.jobs.open-jobs')->with('data',$data);
    }

/*------------------------------------------------
*   viewing a job
*/    
    public function view($id){
        $data = \App\Jobs::where('id', $id)->where('customer_id', Auth::user()->id)->get();
        return view('users.jobs.view-job')->with('data',$data);
    }
/*------------------------------------------------
*   view for posting new job
*/    
    public function post(){

        return view('users.jobs.post-job');
    }

/*------------------------------------------------
*   storing a new job
*/        
    public function storenewpost(Request $request)
    {
        
        $jobs = new \App\Jobs;
        $jobs->title =              $request->input('job-title');
        $jobs->description =        $request->input('job-desc');
        $jobs->budget =             $request->input('job-budget');
        $jobs->experience =         $request->input('exp');
        $jobs->deadline =           $request->input('deadline');
        $jobs->status =             'active';
        $jobs->customer_id =          Auth::user()->id;
        $jobs->skills =             $request->input('skills');
        $jobs->save();

        return view('users.jobs.success');
    }
/*------------------------------------------------
*   viewing all messages
*/        
    public function messages(){
        $connections = \App\Connections::where('cust_id', Auth::user()->id)->get();
        return view('users.messages')->with('data', $connections);
    }

/*------------------------------------------------
*   view a conversation
*/        
    public function messagesview($id){
        $allmsg = \App\UserMessages::where('u_id', $id)->where('u_to_id', Auth::user()->id)->get();
        return view('users.messagesview')->with('data', $allmsg);
    }
/*------------------------------------------------
*   new msg
*/        
    public function send(Request $request){
        $newmsg = new \App\Reply;
        $newmsg->id = hexdec(uniqid());
        $newmsg->m_id = $request->input('lastmgid');
        $newmsg->m_content = $request->input('message');
        $newmsg->save();

        return "done";
    }
}