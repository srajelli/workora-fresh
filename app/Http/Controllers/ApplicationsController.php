<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
use Auth;
use Redirect;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ApplicationsController extends Controller
{
    public function __construct(){
        
        $this->middleware('auth');
    }


/*------------------------------------------------
*   accepting a application
*/
    public function request(Request $request){
        /* add a record in connections table */        
        $accept = new \App\Connections;
        $accept->pro_id     = $request->input('pro_id');
        $accept->cust_id    = $request->input('cust_id');
        $accept->job_id     = $request->input('app_id');
        $accept->save();

        /* change the message type to 0 */
        $msgid = $request->input('msg_id');
        $message = \App\UserMessages::where('m_id', $msgid)->first();
        $message->m_type = 0;
        $message->save();
    } 
}