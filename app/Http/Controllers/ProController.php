<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class ProController extends Controller 
{
	/**
	 * Display the curated jobs for the professional
	 *
	 */
	public function newsfeed()
	{
		$jobs = \App\Jobs::where('skills', 'LIKE', '%web%')->get();
		return view('pros.newsfeed')->with('data', $jobs);	
	}

	/**
	 * Display the specific job
	 *
	 */
    public function view($id){
        $pro = \App\Professionals::where('customer_id', Auth::user()->id)->get();
        $data = \App\Jobs::orderBy('created_at')->where('id', $id)->where('skills', $pro[0]['skills'])->get();
        
        return view('pros.jobs.view-job')->with('data',$data);
    }

	/**
	 * Process the job application 
	 *
	 */
	public function apply(Request $request)
	{				
		$cust = \App\Jobs::where('id', $request->input('id'))->get();
		$pro = \App\Professionals::where('customer_id', Auth::user()->id)->get();

		$application = new \App\Applications;
		$application->pro_id 	= 	$pro[0]['id'];
		$application->job_id 	= 	$request->input('id');
		$application->cust_id 	= 	$cust[0]['customer_id'];
		$application->qoute  	= 	$request->input('quote');

		$application->save();
		$message_id = hexdec(uniqid());
		$message = new \App\Messages;
		$message->id 		= $message_id;
		$message->m_content = $request->input('message');
		$message->save();

		$msg = new \App\UserMessages;
		$msg->u_id 		= Auth::user()->id;
		$msg->u_to_id 	= $cust[0]['customer_id'];
		$msg->m_id 		= $message_id;
		$msg->m_type 	= 1;
		$msg->save();


		return "done";
	}	
	/**
	 * Professionals registration 
	 *
	 */	
	public function registerprofessional(Request $request)
	{
		$newpro = new \App\User;
		$uniqueserid = hexdec(uniqid());
		$newpro->id 		= $uniqueserid;
		$newpro->name 		= $request->input('name');
		$newpro->u_type 	= 1;
		$newpro->email 		= $request->input('email');
		$newpro->password 	= bcrypt($request->input('password'));
		$newpro->mob 		= $request->input('mob');

		$newpro->save();


		$newprodetails = new \App\Professionals;
		
		$newprodetails->customer_id = $uniqueserid;
		$newprodetails->skills 		= $request->input('jobcat');
		$newprodetails->about 		= $request->input('about');
		$newprodetails->location 	= $request->input('location');

		$newprodetails->save();
		return "done";
	}

/*------------------------------------------------
*   viewing all messages
*/        
    public function messages(){
        $messages = \App\UserMessages::where('u_id', Auth::user()->id)->get();
        return view('pros.messages')->with('data', $messages);
    }

/*------------------------------------------------
*   view a conversation
*/        
    public function messagesview($id){
        $allmsg = \App\UserMessages::where('u_id', Auth::user()->id)->where('u_to_id', $id)->get();

        return view('pros.messagesview')->with('data', $allmsg);
    }
/*------------------------------------------------
*   new msg
*/        
    public function send(Request $request){

		$message_id = hexdec(uniqid());
		$message = new \App\Messages;
		$message->id 		= $message_id;
		$message->m_content = $request->input('message');
		$message->save();

		$msg = new \App\UserMessages;
		$msg->u_id 		= Auth::user()->id;
		$msg->u_to_id 	= 2;
		$msg->m_id 		= $message_id;
		$msg->save();
        return "done";
    }
    /**
     * View a pros profile
     *
     */                
    public function profileview($id)
    {
    	$data = \App\User::where('id', $id)->get();
    	return view('profile')->with('data', $data);
    }    
}