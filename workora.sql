-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2015 at 06:13 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `workora`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE IF NOT EXISTS `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `cust_id` int(11) NOT NULL,
  `qoute` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `applications_job_id_unique` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `applications`
--

INSERT INTO `applications` (`id`, `job_id`, `pro_id`, `cust_id`, `qoute`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 2, 7000, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 7, 2, 2, 100000, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `budget` int(11) NOT NULL,
  `location` text COLLATE utf8_unicode_ci NOT NULL,
  `experience` int(11) NOT NULL,
  `deadline` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `skills` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `customer_id`, `title`, `description`, `budget`, `location`, `experience`, `deadline`, `status`, `created_at`, `updated_at`, `skills`) VALUES
(1, '2', 'Nullam accumsan lorem in', 'In consectetuer turpis ut velit. Aliquam lobortis. Curabitur suscipit suscipit tellus. Mauris sollicitudin fermentum libero.\r\n\r\nInteger tincidunt. Praesent congue erat at massa. Pellentesque ut neque. Maecenas vestibulum mollis diam.', 49090, 'Delhi , India', 3, '2015-03-20', 'active', '2015-11-02 16:43:25', '2015-11-02 16:43:25', 'web'),
(2, '2', 'Aliquam lorem ante dapibus', 'Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Fusce vel dui. Donec interdum, metus et hendrerit aliquet, dolor diam sagittis ligula, eget egestas libero turpis vel mi. Phasellus gravida semper nisi. Nam ipsum risus, rutrum vitae, v', 80000, 'Mumbai , India', 2, '2016-04-20', 'active', '2015-11-02 16:56:14', '2015-11-02 16:56:14', ''),
(6, '2', 'Fusce commodo aliquam arcu', 'Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Nam eget dui. Ut id nisl quis enim dignissim sagittis. Phasellus consectetuer vestibulum elit.\r\n\r\nPellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac ', 30000, '', 2, '2016-03-20', 'active', '2015-11-02 17:59:50', '2015-11-02 17:59:50', 'web'),
(7, '2', 'Praesent ut ligula non', 'Fusce pharetra convallis urna. Curabitur nisi. Quisque ut nisi. Etiam ut purus mattis mauris sodales aliquam.\r\n\r\nFusce vel dui. Aenean ut eros et nisl sagittis vestibulum. Etiam sit amet orci eget eros faucibus tincidunt. Nunc nulla.\r\n\r\nCurabitur turpis. ', 800000, '', 3, '2016-04-03', 'active', '2015-11-02 18:18:29', '2015-11-02 18:18:29', 'web');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100000_create_password_resets_table', 1),
('2015_11_02_145334_create_customers_table', 1),
('2015_11_02_145344_create_professionals_table', 1),
('2015_11_02_145404_create_jobs_table', 1),
('2015_11_02_145445_create_applications_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professionals`
--

CREATE TABLE IF NOT EXISTS `professionals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `skills` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ratings` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`),
  UNIQUE KEY `customer_id_2` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `professionals`
--

INSERT INTO `professionals` (`id`, `customer_id`, `skills`, `designation`, `about`, `ratings`, `created_at`, `updated_at`) VALUES
(1, 1, 'web', 'Web Developer', 'Phasellus blandit leo ut odio. Praesent nec nisl a purus blandit viverra. Suspendisse faucibus, nunc et pellentesque egestas, lacus ante convallis tellus, vitae iaculis lacus elit id tortor. Cras id dui.Nullam tincidunt adipiscing enim. Duis lobortis ', 'four', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 3, 'web', 'Application Developer', 'Nullam quis ante. Nulla porta dolor. Praesent nonummy mi in odio. Mauris turpis nunc, blandit et, volutpat molestie, porta ut, ligula.', 'three', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pro` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `mob` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`),
  UNIQUE KEY `customers_mob_unique` (`mob`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `pro`, `email`, `password`, `mob`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shubham Rajelli', 1, 'srajelli81@gmail.com', '$2y$10$qe1/4T7FpFxs2URLTKUnu.nQynACdaIspmyj2q8fLaqUG5wsauhdC', '8080808080', 'PBlmVqlGjm1Ut9UZ6Ik4bhv6p2W14c3ZDa6v2rgjGNIQCzMHwriCmqwF3g7e', '2015-11-02 16:30:57', '2015-11-09 14:29:32'),
(2, 'Saurabh', 0, 'saub@gmail.com', '$2y$10$kCEaNlq.ouXZcHZ/r6wtAOqA19qzdt3gwBZlTLApyMWQvq2ckFUiS', '9090909090', 'OWWKT30FTWHYtngvBQjgq77kUjkVaAOjCl0ZwoA0a3BYKYqplKTOM021b4Dz', '2015-11-02 17:35:30', '2015-11-09 14:20:01'),
(3, 'Shaman Kumar', 1, 'shaman@gmail.com', '$2y$10$MbeXzJogh8Mo/56j09hEk.6xAljFC9oAJOXrcvgBapQ7V7A7ZheIW', '9080808080', NULL, '2015-11-11 04:40:53', '2015-11-11 04:40:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
