@extends('users.app-user')

@section('content')
<!-- Titlebar
================================================== -->

<div id="titlebar" class="resume">
	<div class="container">
		<div class="ten columns">

		<?php
			foreach ($data as $a) {
				$d = \App\Professionals::where('customer_id', $a->id)->get();
				
			}

		?>

		@foreach ($d as $pd)

			<div class="resume-titlebar">
				<img src="{{URL::to('/')}}/images/resumes-list-avatar-01.png" alt="">
				<div class="resumes-list-content">
					<h4>{{$data[0]['name']}} <span>{{$pd->designation}}</span></h4>
					<span class="icons"><i class="fa fa-map-marker"></i> {{$pd->location}}</span>
					
					<span class="icons"><a href="mailto:{{$data[0]['email']}}"><i class="fa fa-envelope"></i> {{$data[0]['email']}}</a></span>
					<div class="skills">
						<span>JavaScript</span>
						<span>PHP</span>
						<span>WordPress</span>
					</div>
					<div class="clearfix"></div>

				</div>
			</div>
		</div>

		<div class="six columns">
			<div class="two-buttons">

		
			</div>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="eight columns">
	<div class="padding-right">

		<h3 class="margin-bottom-15">About Me</h3>

		<p class="margin-reset">
			{{$pd->about}}
		</p>

	</div>
	</div>


	<!-- Widgets -->
	<div class="eight columns">

		<h3 class="margin-bottom-20">Portfolio</h3>

		<!-- Resume Table -->
		<dl class="resume-table">
			<dt>
				<small class="date">2012 - 2015</small>
				<strong>Nottyfood.com | Online Food Delivery Website</strong>
			</dt>
			<dd>
				<p>Captain, why are we out here chasing comets? Maybe we better talk out here; the observation lounge has turned into a swamp. Ensign Babyface!</p>
			</dd>

		
			<dt>
				<small class="date">2006 - 2010</small>
				<strong>Snoden.io | Swiss based Cloud Storage Provider</strong>
			</dt>
			<dd>
				<p>Captain, why are we out here chasing comets? Maybe we better talk out here; the observation lounge has turned into a swamp. Ensign Babyface!</p>
			</dd>


			<dt>
				<small class="date">2004 - 2006</small>
				<strong>Pompi.co | Serious is Nothing ?</strong>
			</dt>
			<dd>
				<p>Phasellus vestibulum metus orci, ut facilisis dolor interdum eget. Pellentesque magna sem, hendrerit nec elit sit amet, ornare efficitur est.</p>
			</dd>

		</dl>

	</div>
	@endforeach
</div>
@endsection
