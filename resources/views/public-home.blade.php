@extends('app')
@section('content')
    <div class="clearfix"></div>
<!-- Banner
================================================== -->
<div id="banner" style="background: url({{URL::to('/')}}/images/bg/home-1.jpg);background-size: cover;">
  <div class="container">
    <div class="sixteen columns">
      
      <div class="search-container">

        <!-- Form -->
        <h2>Find professionals from across India. </h2>
        <form id="search-form" action="{{URL::to('/')}}/search/1/" method="GET">
          <input  type="text" id="location" name="location"  class="ico-02" placeholder="city, province or region" value=""/>
          <input  type="text" id="job" name="job" class="ico-01" placeholder="job title or keywords" value=""/>
          
          <button type="submit"><i class="fa fa-search"></i></button>         
        </form>

        
      </div>

    </div>
  </div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
<div class="container">
  <div class="sixteen columns">
    <h3 style="
    margin-bottom: 45px !important;
    text-align: center;
    font-size: 25px;
    color: rgb(123, 125, 125);
    text-transform: uppercase;
">Workora’s Ocean of Opportunities</h3>
    <ul id="popular-categories">
      <li><a href="#"><i class="fa fa-code"></i> Web Developers </a></li>
      <li><a href="#"><i class="fa fa-line-chart"></i> Accounting / Finance</a></li>
      <li><a href="#"><i class="fa fa-pencil"></i> Writers</a></li>
      <li><a href="#"><i class="fa fa-bar-chart"></i> Sales / Marketing</a></li>
      <li><a href="#"><i class="fa fa-graduation-cap"></i> Legal </a></li>
      <li><a href="#"><i class="fa fa-tablet"></i> Application Developers</a></li>
      <li><a href="#"><i class="fa fa-user"></i> Customer Service </a></li>
      <li><a href="#"><i class="fa fa-server"></i> Server Administrators</a></li>
    </ul>

    <div class="clearfix"></div>
    <div class="margin-top-30"></div>

    <a href="{{URL::to('/')}}/categories" class="button centered">Browse All Categories</a>
    <div class="margin-bottom-50"></div>
  </div>
</div>


<!-- Testimonials -->
<div id="testimonials">
  <!-- Slider -->
  <div class="container">
    <div class="sixteen columns">
      <div class="testimonials-slider">
          <ul class="slides">
            <li>
              <p>This is a much better way to find someone who has what it needs to get the job done.
              <span>Shubham, Tvastra Industries</span></p>
            </li>

            <li>
              <p>We just hired our new Developer and it took us 30 minutes!
              <span>Karan, Visionary Solutions</span></p>
            </li>
          </ul>
      </div>
    </div>
  </div>
</div>

<br>
<!-- Recent Posts -->
<div class="container">
  <div class="sixteen columns">
    <h3 class="margin-bottom-25">Recent Posts</h3>
  </div>


  <div class="one-third column">

    <!-- Post #1 -->
    <div class="recent-post">
      <div class="recent-post-img"><a href="{{URL::to('/')}}/blog/post"><img src="images/blog/launched.jpg" alt="" style="
    height: 253px;
    width: 100%;
"></a><div class="hover-icon"></div></div>
      <a href="#"><h4>It's Official, Workora is launched !</h4></a>
      <div class="meta-tags">
        <span>October 8, 2015</span>
        <span><a href="#">0 Comments</a></span>
      </div>
      <p>The world of job seeking can be all consuming. From secretly stalking the open reqs page of your dream company to sending endless applications.</p>
      <a href="{{URL::to('/')}}/blog/post" class="button">Read More</a>
    </div>

  </div>


  <div class="one-third column">

    <!-- Post #2 -->
    <div class="recent-post">
      <div class="recent-post-img"><a href="{{URL::to('/')}}/blog/post"><img src="images/blog/dream-job.jpg" alt=""></a><div class="hover-icon"></div></div>
      <a href="#"><h4>How to land your Dream Job.</h4></a>
      <div class="meta-tags">
        <span>October 8, 2015</span>
        <span><a href="#">0 Comments</a></span>
      </div>
      <p>Many  college  graduates  and  people  who  want  to  change  careers  have  a  difficult time  getting  the  job  they  want  because  they  lack  experience.</p>
      <a href="{{URL::to('/')}}/blog/post" class="button">Read More</a>
    </div>

  </div>

  <div class="one-third column">

    <!-- Post #3 -->
    <div class="recent-post">
      <div class="recent-post-img"><a href="{{URL::to('/')}}/blog/post"><img src="images/blog/cold-calling.jpg" alt=""></a><div class="hover-icon"></div></div>
      <a href="#"><h4>Cold Calling to increase your sales</h4></a>
      <div class="meta-tags">
        <span>August 27, 2015</span>
        <span><a href="#">0 Comments</a></span>
      </div>
      <p>Loved by some and hated by others, cold calling is one of the best ways to find new customers and clients for your business.</p>
      <a href="{{URL::to('/')}}/blog/post" class="button">Read More</a>
    </div>
  </div>

</div>
@endsection