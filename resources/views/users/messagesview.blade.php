@extends("users.app-user")
@section('content')
<style type="text/css">
/* Bit of normalisation */
header{
	    box-shadow: 0px 1px 9px;
}
/* .bubble */

.bubble {
	background-color: #eee;

	/* vendor rules */
	border-radius: 20px;
	/* vendor rules */
	box-shadow: inset 0 5px 5px rgba(255, 255, 255, 0.4), 0 1px 3px rgba(0, 0, 0, 0.2);
	/* vendor rules */
	box-sizing: border-box;
	clear: both;
	float: left;
	margin-bottom: 20px;
	padding: 8px 30px;
	position: relative;
	text-shadow: 0 1px 1px rgba(255, 255, 255, 0.7);
	width: auto;
	max-width: 100%;
	word-wrap: break-word;
}

.bubble:before, .bubble:after {
	border-radius: 20px / 10px;
	content: '';
	display: block;
	position: absolute;
}

.bubble:before {
	border: 10px solid transparent;
	border-bottom-color: rgba(0, 0, 0, 0.5);
	bottom: 0;
	left: -7px;
	z-index: -2;
}

.bubble:after {
	border: 8px solid transparent;
	border-bottom-color: #eee;
	bottom: 1px;
	left: -5px;
}

.bubble--alt {
	background-color: #AAD6D8;
	float: right;
}

.bubble--alt:before {
	border-bottom-color: rgba(0, 0, 0, 0.5);
	border-radius: 20px / 10px;
	left: auto;
	right: -7px;
}

.bubble--alt:after {
	border-bottom-color: #AAD6D8;
	border-radius: 20px / 10px;
	left: auto;
	right: -5px;
}	
</style>


<div class="container" style="width: 40%;margin-top: 50px;">
	@foreach ($data as $amsg)
		<?php 
		$ms = \App\Messages::where('id', $amsg->m_id)->get(); 
		$customerbubble = \App\Reply::where('m_id', $amsg->m_id)->get();
		?>

		@foreach ($ms as $m)
			<div class="bubble">{{$m->m_content}}</div>
			@foreach ($customerbubble as $cbubble)
					<div class="bubble bubble--alt">{{$cbubble->m_content}}</div>
			@endforeach
		@endforeach
	@endforeach
	<form action="{{URL::to('/')}}/users/messages/send" method="POST">
		<input name="lastmgid" id="lastmgid" type="hidden" value="{{ $amsg->m_id }}">	
		<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">	
		<textarea name="message" id="message"></textarea>
		<button type="submit" class="button big margin-top-5">Send <i class="fa fa-arrow-circle-right"></i></button>
	</form>
</div>
@endsection