<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> 
<!--<![endif]-->
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>Workora.com - Finding the right professional for your job | Hire professionals | Meet Professionals</title>

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{ URL::to('/')}}/css/style.css?ver=1.0000">
<link rel="stylesheet" href="{{ URL::to('/')}}/css/colors/cyan.css" id="colors">


<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

</head>

<body>
<div id="wrapper">


<!-- Header
================================================== -->
<header>
<div class="container">
	<div class="sixteen columns">
	
		<!-- Logo -->
		<div id="logo">
			<h1><a href="{{URL::to('/')}}/home"><img src="{{URL::to('/')}}/images/logo-dark.png" alt="workora" style="
    height: 17px;
"></a></h1>
		</div>

		<!-- Menu -->
		<nav id="navigation" class="menu">
			<ul id="responsive">			
				<li><a href="{{URL::to('/')}}/users/jobs">Jobs</a></li>			
				<li><a href="{{URL::to('/')}}/users/messages">Messages</a></li>
			</ul>

			<ul class="float-right">
				<li><a href="">Hi, {{ Auth::user()->name }}</a>
					<ul>
						<li><a href="{{URL::to('/')}}/users">Profile</a></li>
						<li><a href="{{URL::to('/')}}/auth/logout">Logout</a></li>
					</ul>
				</li>
			</ul>
		</nav>

		<!-- Navigation -->
		<div id="mobile-navigation">
			<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
		</div>

	</div>
</div>
</header>
		@yield('content')

<!-- Footer
================================================== -->
<div class="margin-top-15"></div>

<div id="footer">
	<!-- Main -->
	<div class="container">

		<div class="seven columns">
			<h4>About</h4>
			<p>Workora is a simple platform for employers who wants to hire the best professionals out there and for professionals who want to work with the best</p>
			<a href="#" class="button">Get Started</a>
		</div>

		<div class="three columns">
			<h4>Company</h4>
			<ul class="footer-links">
				<li><a href="#">About Us</a></li>
				<li><a href="#">Careers</a></li>
				<li><a href="#">Our Blog</a></li>
				<li><a href="#">Terms of Service</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Hiring Hub</a></li>
			</ul>
		</div>
		
		<div class="three columns">
			<h4>Press</h4>
			<ul class="footer-links">
				<li><a href="#">In the News</a></li>
				<li><a href="#">Press Releases</a></li>
				<li><a href="#">Awards</a></li>
				<li><a href="#">Testimonials</a></li>
				<li><a href="#">Timeline</a></li>
			</ul>
		</div>		

		<div class="three columns">
			<h4>Browse</h4>
			<ul class="footer-links">
				<li><a href="#">Professionals by Category</a></li>
				<li><a href="#">Professionals in Gujrat</a></li>
				<li><a href="#">Professionals in Banglore</a></li>
				<li><a href="#">Professionals in Mumbai</a></li>
				<li><a href="#">Professionals in Pune</a></li>
				<li><a href="#">Find Jobs</a></li>

			</ul>
		</div>

	</div>

	<!-- Bottom -->
	<div class="container">
		<div class="footer-bottom">
			<div class="sixteen columns">
				<h4>Let's Socialize	</h4>
				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
				<div class="copyrights">©  Copyright 2015 by <a href="#">Workora</a>. All Rights Reserved.</div>
			</div>
		</div>
	</div>

</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script src="{{ URL::to('/')}}/scripts/jquery-2.1.3.min.js"></script>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places"></script>
<script src="{{ URL::to('/')}}/scripts/custom.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.superfish.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.themepunch.tools.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.themepunch.revolution.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.themepunch.showbizpro.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.flexslider-min.js"></script>
<script src="{{ URL::to('/')}}/scripts/chosen.jquery.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.magnific-popup.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/waypoints.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.counterup.min.js"></script>
<script src="{{ URL::to('/')}}/scripts/jquery.jpanelmenu.js"></script>
<script src="{{ URL::to('/')}}/scripts/stacktable.js"></script>
</body>
</html>