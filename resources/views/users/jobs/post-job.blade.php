@extends('users.app-user')

@section('content')
<!-- Titlebar
================================================== -->
<div id="titlebar" class="single submit-page">
	<div class="container">

		<div class="sixteen columns">
			<h2><i class="fa fa-plus-circle"></i> Add Job</h2>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
		
	<!-- Submit Page -->
	<div class="sixteen columns">
		<div class="submit-page">
		<form action="{{URL::to('/')}}/users/jobs/post" method="POST">
			<!-- Notice -->
			<div class="notification notice closeable margin-bottom-40">
				<p><span>Note : </span> Please fill out the job details below to continue</p>
			</div>


			<!-- Title -->
			<div class="form">
				<h5>Job Title</h5>
				<input name="job-title" id="job-title" class="search-field" type="text" placeholder="" value=""/>
			</div>

			<!-- Location -->
			<div class="form">
				<h5>Location <span>(selected)</span></h5>
				<input name="location" name="location" class="search-field" type="text" placeholder="e.g. Mumbai" value="" />
					
			</div>


			<!-- Choose Category -->
			<div class="form">
				<div class="select">
					<h5>Category</h5>
					<select name="skills" id="skills" data-placeholder="Choose Categories" class="chosen-select">
						<option value="web">Web Developer</option>
						<option value="mobile">Mobile Developer</option>
						<option value="cs">Company Secretary</option>
						<option value="content">Content Writers</option>
						<option value="sales">Sales & Marketing Experts</option>
					</select>
				</div>
			</div>
			<div class="select">
			<h5>Select Experience: </h5>
			<select name="exp" id="exp" data-placeholder="Select Experience Level" class="chosen-select">
				<option value="1">Basic</option>
				<option value="2">Intermediate</option>
				<option value="3">Experienced</option>
			</select>
			</div>

			<!-- Tags -->
			<div class="form">
				<br>
				<h5>Budget </h5>
				<input name="job-budget" name="job-budget" class="search-field" type="text" placeholder="₹ 10000" value=""/>
			</div>


			<!-- Description -->
			<div class="form">
				<h5>Description</h5>
				<textarea class="WYSIWYG" name="job-desc" cols="40" rows="3" id="job-desc" spellcheck="true"></textarea>
			</div>


			<!-- TClosing Date -->
			<div class="form">
				<h5>Closing Date <span>(optional)</span></h5>
				<input name="deadline" name="deadline" data-role="date" type="text" placeholder="yyyy-mm-dd">
				<p class="note">Deadline for new applicants.</p>
			</div>

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
			<div class="divider margin-top-0"></div>
			<button type="submit" class="button big margin-top-5">Submit <i class="fa fa-arrow-circle-right"></i></button>
		</form>
		</div>
	</div>

</div>

@endsection
