@extends('users.app-user')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2>Manage Jobs</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li>Job Dashboard</li>
				</ul>
			</nav>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	
	<!-- Table -->
	<div class="sixteen columns">
		<a href="{{URL::to('/')}}/users/jobs/post" class="button margin-bottom-25">Post a new Job</a>
		<table class="manage-table responsive-table">

			<tr>
				<th>Title</th>
				<th>Date Posted</th>
				<th>Date Expires</th>
				<th>Applications</th>
				<th></th>
			</tr>
					
					
			<!-- Item #2 -->

	@foreach ($data as $ejob)
		<tr>
			<td class="title">
				<a href="{{ URL::to("/") }}/users/jobs/view/{{ $ejob->id }}">{{$ejob->title}}</a>
			</td>
			<td>{{ $ejob->created_at }}</td>
			<td>{{ $ejob->deadline }}</td>
			<td class="centered">
				<a href="{{ URL::to('/') }}/users/applications?jobid={{ $ejob->id }}" class="button">Show</a>
			</td>
			<td class="action">
				<a href="#" class="delete"><i class="fa fa-remove"></i> Delete</a>
			</td>
		</tr>		
	@endforeach
		</table>

		<br>
		

	</div>

</div>


@endsection
