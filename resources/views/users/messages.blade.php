@extends('users.app-user')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2>Messages</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li>Job Dashboard</li>
				</ul>
			</nav>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	
	<!-- Table -->
	<div class="sixteen columns">
		<table class="manage-table responsive-table">
			<tr>
				<th>From</th>
				<th>Message</th>
			</tr>
					
			<!-- Item #2 -->
			
			@foreach($data as $connection)
				<?php 
					$accepted_connections = \App\UserMessages::where('u_id', $connection->pro_id)->where('u_to_id', $connection->cust_id)->where('m_type', 0)->get(); 
					$name = \App\User::where('id', $connection->pro_id)->get();
				?>
				@foreach($accepted_connections as $each_connection)
					<?php $messages = \App\Messages::where('id', $each_connection->m_id)->get(); ?>
					@foreach($messages as $msg)
						@foreach($name as $pros_name)						
						<tr>
						<td>{{$pros_name->name}}</td>
						@endforeach
						<td><a href="{{URL::to('/')}}/users/messages/view/{{$pros_name->id}}">{{$msg->m_content}}</a></td>
						</tr>
					@endforeach
				@endforeach
			@endforeach	
			

		</table>
	</div>

</div>


@endsection
