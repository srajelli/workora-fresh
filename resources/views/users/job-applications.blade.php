@extends('users.app-user')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2>Manage Connections</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="{{ URL::to('/') }}/users/jobs">Jobs</a></li>
					<li>Connections</li>
				</ul>
			</nav>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
	<!-- Table -->
<div class="container">


	
	<!-- Applications -->
	<div class="thirteen columns">
				<div id="noapplicants" class="application" style="display:none;">
					<div class="app-content">
						<!-- Name / Avatar -->
						<div class="info">
							<span style="">No one has applied for the job.</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

				@foreach ($data as $applications)	
					<?php $get_name = DB::select("SELECT users.name, professionals.ratings FROM users JOIN professionals ON users.id = professionals.customer_id WHERE professionals.id = ?",[$applications->pro_id]); ?>
					@foreach ($get_name as $applicants)
						<?php $pdas = \App\Professionals::where('id', $applications->pro_id)->get(); ?>
						@foreach ($pdas as $piddu)				
							<?php $get_msg = \App\UserMessages::where('u_id', $piddu->customer_id)->where('m_type', 1)->get(); ?>
							@foreach($get_msg as $msg)
								<?php $msgcontent = \App\Messages::where('id', $msg->m_id)->get(); ?>
								@foreach ($msgcontent as $mm)
									<div class="jobid-{{ $applications->job_id }} application" style="display:none;">
										<div class="app-content">
											<!-- Name / Avatar -->	
											<div class="info">
												<img src="{{URL::to('/')}}/images/avatar-placeholder.png" alt="">
												<span><a href="{{URL::to('/')}}/pros/profile/view/{{$piddu->customer_id}}">{{ $applicants->name }}</a></span>
											</div>
											
											<!-- Buttons -->
											<div class="buttons">
												<form action="{{URL::to('/')}}/users/connections/" method="GET"> 
													<input type="hidden" name="pro_id" 	id="pro_id" 	value="{{$piddu->customer_id}}">		
													<input type="hidden" name="cust_id" id="cust_id" 	value="{{$applications->cust_id}}">		
													<input type="hidden" name="request" id="request" 	value="accept">		
													<input type="hidden" name="app_id" 	id="app_id" 	value="{{$applications->id}}">		
													<input type="hidden" name="msg_id" 	id="msg_id" 	value="{{$mm->id}}">		

													<button type="submit" class="button gray"><i class="fa fa-check"></i></button>
												</form>

												<form action="{{URL::to('/')}}/users/connections/" method="GET">
													<input type="hidden" name="pro_id" 	id="pro_id" 	value="{{$piddu->customer_id}}">		
													<input type="hidden" name="cust_id" id="cust_id" 	value="{{$applications->cust_id}}">		
													<input type="hidden" name="request" id="request" 	value="reject">		
													<input type="hidden" name="app_id" 	id="app_id" 	value="{{$applications->id}}">														
													<a href="{{URL::to('/')}}/users/applications/reject/{{$applications->id}}" class="button gray app-link"><i class="fa fa-times"></i></a>
												</form>
													<a href="#msg-div" class="button gray app-link"><i class="fa fa-sticky-note"></i> View Message</a>
											</div>
											<div class="clearfix"></div>

										</div>

										<!--  Hidden Tabs -->
										<div class="app-tabs">

											<a href="#" class="close-tab button gray"><i class="fa fa-close"></i></a>
											
											<!-- First Tab -->
										    <div class="app-tab-content" id="msg-div">

												<div class="">
														<p>{{$mm->m_content}}</p>
												</div>

												<div class="clearfix"></div>

										    </div>
										  

										</div>

										<!-- Footer -->
										<div class="app-footer">

											<div class="rating {{ $applicants->ratings }}-stars">
												<div class="star-rating"></div>
												<div class="star-bg"></div>
											</div>

											<ul>
												<li>Amount Quoted: ₹ {{ $applications->qoute }}</li>
												<li><i class="fa fa-calendar"></i> {{$applications->created_at}}</li>
											</ul>
											<div class="clearfix"></div>

										</div>
									</div>										
								@endforeach										
							@endforeach
						@endforeach
					@endforeach
				@endforeach
	</div>
	<script type="text/javascript">
		function getParameter(paramName) {
		  var searchString = window.location.search.substring(1),
		      i, val, params = searchString.split("&");

		  for (i=0;i<params.length;i++) {
		    val = params[i].split("=");
		    if (val[0] == paramName) {
		      return val[1];
		    }
		  }
		  return null;
		}

		var jobid = getParameter('jobid');
		var vis = document.getElementsByClassName("jobid-"+jobid);
		

		if (vis == null)
		{
			var notify = document.getElementById("noapplicants");
			notify.style.display = 'block';
		}
		else
		{
			
			for (var i=0;i<vis.length;i+=1){
			  vis[i].style.display = 'block';
			}			
		}		
	</script>

	<div class="three columns">

			<!-- Sort by -->
			<div class="widget">
				<h4>Sort by</h4>

				<!-- Select -->
				<select data-placeholder="Choose Category" class="chosen-select-no-single" style="display: none;">
					<option selected="selected" value=""></option>
					<option value="raingshigh">Highest Ratings</option>
					<option value="ratehigh">Qoutation – Highest First</option>
					<option value="ratelow">Qoutation – Lowest First</option>
				</select>


			</div>


			<!-- Experience -->
			<div class="widget">
				<h4>Experience</h4>

				<ul class="checkboxes">
					<li>
						<input id="check-1" type="checkbox" name="check" value="check-1" checked="" class="first">
						<label for="check-1">Entry Level</label>
					</li>
					<li>
						<input id="check-2" type="checkbox" name="check" value="check-2">
						<label for="check-2">Intermediate</label>
					</li>
					<li>
						<input id="check-3" type="checkbox" name="check" value="check-3">
						<label for="check-3">Professional</label>
					</li>
				</ul>

			</div>

	</div>	
</div>


@endsection
