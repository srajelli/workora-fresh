@extends('app')
@section('content')
<div class="clearfix"></div>
<!-- Banner
================================================== -->
<div id="banner" style="background: url({{URL::to('/')}}/images/bg/home-1.jpg);background-size: cover;">
	<div class="container">
		<div class="sixteen columns">
			
			<div class="search-container">

				<!-- Form -->
				<h2>Find the right professional for you job</h2>
				<input type="text" name="location"  class="ico-02" placeholder="city, province or region" value=""/>				
				<input type="text" name="job-title" class="ico-01" placeholder="job title, keywords or company name" value=""/>
				<button><i class="fa fa-search"></i></button>

				<!-- Browse Jobs -->
				<div class="browse-jobs">
					Browse job offers by <a href="browse-categories.html"> category</a> or <a href="#">location</a>
				</div>
				
			</div>

		</div>
	</div>
</div>


<!-- Content
================================================== -->

<!-- Categories -->
<div class="container">
	<div class="sixteen columns">
		<h3 style="
    margin-bottom: 45px !important;
    text-align: center;
    font-size: 25px;
    color: rgb(123, 125, 125);
    text-transform: uppercase;">Building alliances beyond profession</h3>
		<ul id="popular-categories">
			<li><a href="#"><i class="fa fa-code"></i> Web Developers </a></li>
			<li><a href="#"><i class="fa fa-line-chart"></i> Accounting / Finance</a></li>
			<li><a href="#"><i class="fa fa-pencil"></i> Writers</a></li>
			<li><a href="#"><i class="fa fa-bar-chart"></i> Sales / Marketing</a></li>
			<li><a href="#"><i class="fa fa-graduation-cap"></i> Legal </a></li>
			<li><a href="#"><i class="fa fa-tablet"></i> Application Developers</a></li>
			<li><a href="#"><i class="fa fa-user"></i> Customer Service </a></li>
			<li><a href="#"><i class="fa fa-server"></i> Server Administrators</a></li>
		</ul>

		<div class="clearfix"></div>
		<div class="margin-top-30"></div>

		<a href="{{URL::to('/')}}/categories" class="button centered">Browse All Categories</a>
		<div class="margin-bottom-50"></div>
	</div>
</div>


<!-- Testimonials -->
<div id="testimonials">
	<!-- Slider -->
	<div class="container">
		<div class="sixteen columns">
			<div class="testimonials-slider">
				  <ul class="slides">
				    <li>
				      <p>This is a much better way to find someone who has what it needs to get the job done.
				      <span>Shubham, Tvastra Industries</span></p>
				    </li>

				    <li>
				      <p>We just hired our new Developer and it took us 30 minutes!
				      <span>Karan, Visionary Solutions</span></p>
				    </li>
				    
				    <li>
				      <p>The best tool for startups to build their team
				      <span>Rohit, RDM Universal</span></p>
				    </li>

				  </ul>
			</div>
		</div>
	</div>
</div>


<!-- Infobox -->
<div class="infobox">
	<div class="container">
		<div class="sixteen columns" style="
    font-size: 26px;
">Searching for a  ?&nbsp;&nbsp;Get Hired<a href="my-account.html">Get Started</a></div>
	</div>
</div>


<!-- Recent Posts -->
<div class="container">
	<div class="sixteen columns">
		<h3 class="margin-bottom-25">Recent Posts</h3>
	</div>


	<div class="one-third column">

		<!-- Post #1 -->
		<div class="recent-post">
			<div class="recent-post-img"><a href="blog-single-post.html"><img src="images/recent-post-01.jpg" alt=""></a><div class="hover-icon"></div></div>
			<a href="blog-single-post.html"><h4>It's Official, Workora is launched !</h4></a>
			<div class="meta-tags">
				<span>October 8, 2015</span>
				<span><a href="#">0 Comments</a></span>
			</div>
			<p>The world of job seeking can be all consuming. From secretly stalking the open reqs page of your dream company to sending endless applications.</p>
			<a href="blog-single-post.html" class="button">Read More</a>
		</div>

	</div>


	<div class="one-third column">

		<!-- Post #2 -->
		<div class="recent-post">
			<div class="recent-post-img"><a href="blog-single-post.html"><img src="images/recent-post-02.jpg" alt=""></a><div class="hover-icon"></div></div>
			<a href="blog-single-post.html"><h4>How to land your Dream Job.</h4></a>
			<div class="meta-tags">
				<span>September 12, 2015</span>
				<span><a href="#">0 Comments</a></span>
			</div>
			<p>Struggling to find your significant other the perfect Valentine’s Day gift? If I may make a suggestion: woo a recruiter. </p>
			<a href="blog-single-post.html" class="button">Read More</a>
		</div>

	</div>

	<div class="one-third column">

		<!-- Post #3 -->
		<div class="recent-post">
			<div class="recent-post-img"><a href="blog-single-post.html"><img src="images/recent-post-03.jpg" alt=""></a><div class="hover-icon"></div></div>
			<a href="blog-single-post.html"><h4>11 Tips to Help You Get New Clients Through Cold Calling</h4></a>
			<div class="meta-tags">
				<span>August 27, 2015</span>
				<span><a href="#">0 Comments</a></span>
			</div>
			<p>If your dream employer appears on this list, you’re certainly in good company. But it also means you’re up for some intense competition.</p>
			<a href="blog-single-post.html" class="button">Read More</a>
		</div>
	</div>

</div>
@endsection
