@extends('app')
@section('content')
<div class="clearfix"></div>

<style type="text/css">
	header{
		box-shadow: 0px 1px 9px;
	}
</style>
<div class="container" style="
    padding-top: 45px;
">

	<!-- Blog Posts -->
	<div class="eleven columns">
		<div class="padding-right">

			<!-- Post -->
			<div class="post-container">
				<div class="post-img"><a href="#"><img src="http://localhost:8000/images/blog/cold-calling.jpg" alt=""></a></div>
				<div class="post-content">
					<a href="#"><h3>Cold Calling Tips To Increase Your Sales</h3></a>
					<div class="meta-tags">
						<span>September 12, 2015</span>
						<span><a href="#">0 Comments</a></span>
					</div>
					<div class="clearfix"></div>
					<div class="margin-bottom-25"></div>

					<p>Loved by some and hated by others, cold calling is one of the best ways to find new customers and clients for your business. Equal parts terrifying and exciting, there’s nothing quite like the feeling of closing a lucrative sale after cold calling someone</p>

					<div class="post-quote">
						<span class="icon"></span>
						<blockquote>Cold calling is the most powerful weapon in any salesperson’s repertoire.

</blockquote>
					</div>

					
					<p>With online marketing becoming the default choice for most businesses looking to rapidly grow their customer base, cold calling has taken a backseat over the last 10 years. However, it remains a hugely profitable form of marketing when done right.</p><p class="margin-reset">In this blog post, you’ll learn 10 tips and tricks to increase your cold calling success rate and drive new business over the phone. From research tips to tricks for getting the right people on then phone, read on to become a cold calling master.</p>

				</div>
			</div>

			<!-- Comments -->
			


			
			


			
			
			
			
			
			

		</div>
	</div>
	<!-- Blog Posts / End -->


	<!-- Widgets -->
	<div class="five columns blog">

		<!-- Search -->
		<div class="widget">
			<h4>Search</h4>
			<div class="widget-box search">
				<div class="input"><input class="search-field" type="text" placeholder="To search type and hit enter" value="" autocomplete="off"></div>
			</div>
		</div>

		<div class="widget">
			<h4>Got any questions?</h4>
			<div class="widget-box">
				<p>If you are having any questions, please feel free to ask.</p>
				<a href="{{URL::to('/')}}/contact" class="button widget-btn"><i class="fa fa-envelope"></i> Drop Us a Line</a>
			</div>
		</div>

		<!-- Tabs -->
		<div class="widget">

			<ul class="tabs-nav blog">
				<li class="active"><a href="#tab1">Popular</a></li>
				<li class=""><a href="#tab2">Featured</a></li>
				<li><a href="#tab3">Recent</a></li>
			</ul>

			<!-- Tabs Content -->
<div class="tabs-container">

				<div class="tab-content" id="tab1" style="display: block;">
					
					<!-- Recent Posts -->
					<ul class="widget-tabs">
						
						<!-- Post #1 -->
						<li>
							<div class="widget-thumb">
								<a href="blog-single-post.html"><img src="http://localhost:8000/images/blog/launched.jpg" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="blog-single-post.html">It's Official, Workora is launched !</a></h5>
								<span>October 8, 2015</span>
							</div>
							<div class="clearfix"></div>
						</li>
						
						<!-- Post #2 -->
						<li>
							<div class="widget-thumb">
								<a href="blog-single-post.html"></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="blog-single-post.html">How to get your dream job</a></h5>
								<span>October 8, 2015</span>
							</div>
							<div class="clearfix"></div>

						</li>
					</ul>
		
				</div>

				<div class="tab-content" id="tab2" style="display: none;">
				
					<!-- Featured Posts -->
					<ul class="widget-tabs">

						<!-- Post #1 -->
						<li>
							<div class="widget-thumb">
								<a href="blog-single-post.html"><img src="images/blog-widget-02.png" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="blog-single-post.html">Hey Job Seeker, It’s Time To Get Up And Get Hired</a></h5>
								<span>October 10, 2015</span>
							</div>
							<div class="clearfix"></div>

						</li>
						
						<!-- Post #2 -->
						<li>
							<div class="widget-thumb">
								<a href="blog-single-post.html"><img src="images/blog-widget-01.png" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="blog-single-post.html">How to "Woo" a Recruiter and Land Your Dream Job</a></h5>
								<span>September 12, 2015</span>
							</div>
							<div class="clearfix"></div>
						</li>
					
					</ul>
				</div>

				<div class="tab-content" id="tab3" style="display: none;">
				
					<!-- Recent Posts -->
					<ul class="widget-tabs">
						
						<!-- Post #1 -->
						<li>
							<div class="widget-thumb">
								<a href="blog-single-post.html"><img src="images/blog-widget-03.png" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="blog-single-post.html">11 Tips to Help You Get New Clients Through Cold Calling</a></h5>
								<span>August 27, 2015</span>
							</div>
							<div class="clearfix"></div>
						</li>
						
						<!-- Post #2 -->
						<li>
							<div class="widget-thumb">
								<a href="blog-single-post.html"><img src="images/blog-widget-02.png" alt=""></a>
							</div>
							
							<div class="widget-text">
								<h5><a href="blog-single-post.html">Hey Job Seeker, It’s Time To Get Up And Get Hired</a></h5>
								<span>October 10, 2015</span>
							</div>
							<div class="clearfix"></div>

						</li>
						
					</ul>
				</div>
				
			</div>
		</div>


		<div class="widget">
			<h4>Social</h4>
				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
		</div>
		
		<div class="clearfix"></div>
		<div class="margin-bottom-40"></div>

	</div>
	<!-- Widgets / End -->


</div>

@endsection