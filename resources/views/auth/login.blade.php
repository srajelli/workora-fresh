@extends('app')

@section('content')
<div class="clearfix"></div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<div class="my-account" style="padding-top:40px;width:430px;">

		<div class="tabs-container">
			<!-- Login -->
			<div class="tab-content" id="tab1" style="display: none;">

				<h3 class="margin-bottom-10 margin-top-10">Login</h3>
				<hr>
				<form method="post" class="login">
	@if (count($errors) > 0)
			<div class="notification error closeable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
			</div>
		@endif
		
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<p class="form-row form-row-wide">
						<label for="username">Username or Email Address:</label>
						<input type="email" class="input-text" name="email" value="">
					</p>

					<p class="form-row form-row-wide">
						<label for="password">Password:</label>
						<input type="password" class="input-text" name="password" value="">
					</p>

					<p class="form-row">
						<input type="submit" class="button" name="login" value="Login" />

						<label for="rememberme" class="rememberme">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label>
					</p>

					<p class="lost_password">
						<a href="#" >Lost Your Password?</a>
					</p>

					
				</form>
			</div>
		</div>
	</div>
</div>

@endsection
