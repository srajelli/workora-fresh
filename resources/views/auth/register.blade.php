@extends('app')

@section('content')
<div class="clearfix"></div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<div class="my-account" style="padding-top:40px;width:430px;">
	@if (count($errors) > 0)
			<div class="notification error closeable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
			</div>
		@endif

		<div class="tabs-container">
			<!-- Login -->
			<div class="tab-content" id="tab1" style="display: none;">
					<h3 class="margin-bottom-10 margin-top-10">Register</h3>
					<hr>

					<form class="register" role="form" method="POST" action="/auth/register">
						<input type="hidden" 				name="_token" value="{{ csrf_token() }}">
						<p class="form-row form-row-wide">
							<label for="reg_email">Name:</label>
							<input type="text" class="input-text" name="name" id="reg_email" value="" />
						</p>

						<p class="form-row form-row-wide">
							<label for="reg_email">Mobile:</label>
							<input type="text" class="input-text" name="mob" id="reg_email" value="" />
						</p>

						<p class="form-row form-row-wide">
							<label for="reg_email">Email Address:</label>
							<input type="email" class="input-text" name="email" id="reg_email" value="" />
						</p>

						
						<p class="form-row form-row-wide">
							<label for="reg_password">Password:</label>
							<input type="password" class="input-text" name="password" id="reg_password" />
						</p>

						<p class="form-row form-row-wide">
							<label for="reg_password2">Repeat Password:</label>
							<input type="password" class="input-text" name="password_confirmation" id="reg_password2" />
						</p>
						
						<p class="form-row">
							<input type="submit" class="button" name="register" value="Register" />
						</p>
						
					</form>

			</div>

		</div>
	</div>
</div>

@endsection
