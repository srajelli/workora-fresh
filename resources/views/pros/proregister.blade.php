@extends('app')

@section('content')
<div class="clearfix"></div>


<!-- Content
================================================== -->

<!-- Container -->
<div class="container">

	<div class="my-account" style="padding-top:40px">
	@if (count($errors) > 0)
			<div class="notification error closeable">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
			</div>
		@endif

		<div class="sixteen columns">
			<h3 class="margin-bottom-10 margin-top-10">Register as a Professional</h3>
			<hr>		
			<div class="five columns">
				<div class="tabs-container">
							<form class="register" role="form" method="POST" action="/professionals/post">
								<input type="hidden" 				name="_token" value="{{ csrf_token() }}">
								<p class="form-row form-row-wide">
									<label for="reg_email">Name:</label>
									<input type="text" class="input-text" name="name" id="name" value="" />
								</p>

								<p class="form-row form-row-wide">
									<label for="reg_email">Mobile:</label>
									<input type="text" class="input-text" name="mob" id="mob" value="" />
								</p>

								<p class="form-row form-row-wide">
									<label for="reg_email">Email Address:</label>
									<input type="email" class="input-text" name="email" id="email" value="" />
								</p>

								
								<p class="form-row form-row-wide">
									<label for="reg_password">Password:</label>
									<input type="password" class="input-text" name="password" id="password" />
								</p>

								<p class="form-row form-row-wide">
									<label for="reg_password2">Repeat Password:</label>
									<input type="password" class="input-text" name="password_confirmation" id="reg_password2" />
								</p>
								
							
				</div>							
			</div>

			<div class="five columns">
								<input type="hidden" 				name="_token" value="{{ csrf_token() }}">
								<p class="form-row form-row-wide">
									<label for="jobcat">Job Category : </label>
									<select name="jobcat" id="jobcat" data-placeholder="Select Job Category" class="chosen-select">
										<option value="web">Web Developer</option>
										<option value="mobile">Mobile Developer</option>
										<option value="cs">Company Secretary</option>
										<option value="content">Content Writers</option>
										<option value="sales">Sales & Marketing Experts</option>
									</select>
								</p>

								<p class="form-row form-row-wide">
									<label for="location">Location : </label>
									<input type="text" id="location" name="location"  class="ico-02" placeholder="city, province or region" value=""/>
								</p>

								<p class="form-row form-row-wide">
									<label for="about">About :</label>
									<textarea name="about" id="about"></textarea>
								</p>

								<p class="form-row">
									<input type="submit" class="button" name="register" value="Register" />
								</p>
				</form>
			</div>			

			<div class="five columns">
				<div class="notification notice closeable">
					<p>For any questions picking your tiny brain, feel free to just shoot us a crisp mail (no riddles please) at 
<span>help@workora.com</span>. Our success manager is eager to hear from you!</p>
					<a class="close" href="#"></a>
				</div>
			</div>						
		</div>
	</div>
</div>

@endsection
