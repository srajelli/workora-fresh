@extends('app')

@section('content')
<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="sixteen columns">
			<h2>Are you looking for work that challenges your creative senses and also pays well?</h2>
			<br>	
			<span>There is always a tradeoff between creativity, freedom of work and the pay. At Workora, we only 
encourage great work and great pay for you. And the variety? You just name it!</span>			
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<div class="sixteen columns">

		<div class="info-banner">
			<div class="info-content">
				<h3 style="text-align:center">With Workora, finding the right client just got a million times easier</h3>
				<br>
				<p>No long waits and no bidding. Find what you like? Just get connected and there is no looking back! All you need to do is, deliver your service in the most efficient manner with highest quality and make your clients fall for you.</p>
			</div>
			<div class="clearfix"></div>
				<br>
				<h3 style="text-align:center">How it Works</h3>			
				<br>
				<p>We knew you couldn’t resist it :-D Getting started on WorkOra is as simple as a pinch of salt.Pick the work of your choice from our vast range of options in your news feed. Earn credible connections and seal the deal in just 3 simple steps.</p>
		</div>
		</div>
		<div class="plan color-1 one-third column">
				<div class="plan-price">
					<h3>Choose</h3>
					<span style="font-size: 20px;">Choose leads which fits your skills</span>
				</div>
		</div>

		<div class="plan color-1 one-third column">
				<div class="plan-price">
					<h3>Connect</h3>
					<span style="font-size: 20px;">Apply for a connect with the client</span>
				</div>
		</div>

		<div class="plan color-1 one-third column">
				<div class="plan-price">
					<h3>Convert</h3>
					<span style="font-size: 20px;">Chat with the client and win the project!</span>
				</div>
		</div>


		<p style="text-align:center;"><a class="button" href="#"><i class="fa fa-user"></i> Get started</a></p>
	
</div>

@endsection
