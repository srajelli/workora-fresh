@extends('pros.app-pro')

@section('content')
<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="ten columns">
			<span>We found 1,412 jobs matching:</span>
			<h2>Web, Software & IT</h2>
		</div>
	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
		
		<ul class="job-list full">
			@foreach($data as $job)
			<li>
				<a href="{{URL::to('/')}}/pros/jobs/view/{{$job['id']}}">
					<img src="{{URL::to('/')}}/images/job-list-logo-01.png" alt="">
					<div class="job-list-content">
						<h4>{{$job['title']}}</h4>
						<div class="job-icons">
							<span><i class="fa fa-map-marker"></i>{{$job['location']}}</span>
							<span>&nbsp;&nbsp;Budget: ₹ {{$job['budget']}}</span>
						</div>
						<p>{{$job['description']}}</p>
					</div>
				</a>
				<div class="clearfix"></div>
			</li>
			@endforeach
		</ul>
		<div class="clearfix"></div>


	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Sort by</h4>

			<!-- Select -->
			<select data-placeholder="Choose Category" class="chosen-select-no-single">
				<option selected="selected" value="recent">Newest</option>
				<option value="oldest">Oldest</option>
				<option value="expiry">Expiring Soon</option>
				<option value="ratehigh">Hourly Rate – Highest First</option>
				<option value="ratelow">Hourly Rate – Lowest First</option>
			</select>

		</div>



	</div>
	<!-- Widgets / End -->


</div>
@endsection
