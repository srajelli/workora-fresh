@extends('users.app-user')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2>Manage Jobs</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li>Job Dashboard</li>
				</ul>
			</nav>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	
	<!-- Table -->
	<div class="sixteen columns">

		<p class="margin-bottom-25">Your listings are shown in the table below. Expired listings will be automatically removed after 30 days.</p>

		<table class="manage-table responsive-table">

			<tr>
				<th>Title</th>
				<th>Filled?</th>
				<th>Date Posted</th>
				<th>Date Expires</th>
				<th>Applications</th>
				<th></th>
			</tr>
					
					
			<!-- Item #2 -->
<?php 

	foreach ($data as $ejob ) {
		//print_r($ejob);
		echo "<tr>";
			$url = URL::to("/");
			echo "<td class=\"title\"><a href=\"".$url."/users/jobs/view/".$ejob["id"]."\">".$ejob["title"]."</a></td>";
			echo "<td class=\"centered\">-</td>";
			echo "<td>".$ejob["created_at"]."</td>";
			echo "<td>".$ejob["deadline"]."</td>";
			echo "<td class=\"centered\"><a href=\"#\" class=\"button\">Show</a></td>";
			echo "<td class=\"action\">";
			echo "<a href=\"#\"><i class=\"fa fa-pencil\"></i> Edit</a>";
			echo "<a href=\"#\"><i class=\"fa  fa-check \"></i> Mark Filled</a>";
			echo "<a href=\"#\" class=\"delete\"><i class=\"fa fa-remove\"></i> Delete</a>";
			echo "</td>";
		echo "</tr>";
	}
?>

		</table>

		<br>
		<a href="{{URL::to('/')}}/users/jobs/post" class="button">Add Job</a>

	</div>

</div>


@endsection
