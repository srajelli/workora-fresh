@extends('pros.app-pro')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="eleven columns">
			<h2><?php echo $data[0]['title']; ?></h2>
		</div>

		<div class="six columns">
			
		</div>

	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
	
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
		
		<!-- Company Info -->
		<div class="company-info">
			<img src="{{URL::to('/')}}/images/company-logo.png" alt="">
			<div class="content">
<?php 
	
	$employer = DB::table('users')->where('id', $data[0]['customer_id'])->get();
	//print_r($employer);
	
	foreach ($employer as $empll) {
		//print_r($empll);
		echo "<h4> Posted by: ".$empll->name."</h4>";
	}
?>			
			</div>
			<div class="clearfix"></div>
		</div>

		<p class="margin-reset">
			<?php echo $data[0]['description']; ?>
		</p>

	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Overview</h4>

			<div class="job-overview">
				
				<ul>
					<li>
						<i class="fa fa-map-marker"></i>
						<div>
							<strong>Location:</strong>
							<span><?php echo $data[0]['location'];?></span>
						</div>
					</li>
					<li>
						<i class="fa fa-clock-o"></i>
						<div>
							<strong>Deadline:</strong>
							<span><?php echo $data[0]['deadline'];?></span>
						</div>
					</li>
					<li>
						<i class="fa fa-rupee"></i>
						<div>
							<strong>Budget:</strong>
							<span>₹ <?php echo $data[0]['budget'];?></span>
						</div>
					</li>
				</ul>


				<a href="#small-dialog" class="popup-with-zoom-anim button">Apply For This Job</a>

				<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
					<div class="small-dialog-headline">
						<h2>Apply For This Job</h2>
					</div>

					<div class="small-dialog-content">
						<form action="{{URL::to('/')}}/pros/jobs/apply" method="POST">
							<textarea name="message" id="message" placeholder="Your message / cover letter sent to the employer"></textarea>
							<input name="quote" id="quote" type="text" placeholder="Quotation" value=""/>
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<input type="hidden" name="id" id="id" value="<?php echo $data[0]['id'];?>">
							<button class="send">Send Application</button>
						</form>
					</div>
					
				</div>

			</div>

		</div>

	</div>
	<!-- Widgets / End -->


</div>
@endsection
