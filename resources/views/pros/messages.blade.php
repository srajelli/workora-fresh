@extends('pros.app-pro')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
	<div class="container">

		<div class="sixteen columns">
			<h2>Messages</h2>
			<nav id="breadcrumbs">
				<ul>
					<li>You are here:</li>
					<li><a href="#">Home</a></li>
					<li>Job Dashboard</li>
				</ul>
			</nav>
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	
	<!-- Table -->
	<div class="sixteen columns">

		<table class="manage-table responsive-table">

			<tr>
				<th>From</th>
				<th>Message</th>
				<th>Date</th>
				<th>Action</th>
			</tr>
					
					

			<!-- Item #2 -->

<?php
	foreach ($data as $emsg) 
	{
		$from = \App\User::where('id', $emsg->u_to_id)->get();
		$msg = \App\Reply::where('m_id', $emsg->m_id)->get();
	
		foreach ($msg as $m) {
		  	echo "<tr>";
		  		foreach ($from as $f) {
		  			echo "<td>".$f->name."</td>";
		  		}
		  		echo "<td>".$m->m_content."</td>";
		  		echo "<td>".$m->created_at."</td>";
		  		echo "<td><a href=\"".URL::to('/')."/pros/messages/view/".$emsg->u_to_id."\" class=\"button\">Show</a></td>";
		  	echo "</tr>";
		  }  
	}
?>

		</table>
	</div>

</div>


@endsection
