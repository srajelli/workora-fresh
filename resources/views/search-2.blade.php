@extends('app')
@section('content')

<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
	<div class="my-account" style="padding-top:40px">

		<ul class="tabs-nav">
			<li><a href="#tab1">Register</a></li>
			<li class=""><a href="#tab2">Login</a></li>			
		</ul>

		<div class="tabs-container">

			<!-- Register -->
			<div class="tab-content" id="tab1" style="display: none;">

					<h3 class="margin-bottom-10 margin-top-10">Register</h3>

					<form method="post" action="{{URL::to('/')}}/auth/register/" class="register">
						<p class="form-row form-row-wide">
							<label for="name">Name:</label>
							<input type="text" class="input-text" name="name" value="">
						</p>

			
						<p class="form-row form-row-wide">
							<label for="email">Email Address:</label>
							<input type="email" class="input-text" name="email" value="">
						</p>

						<p class="form-row form-row-wide">
							<label for="mobile">Mobile:</label>
							<input type="text" class="input-text" name="mob" value="">
						</p>

						<p class="form-row form-row-wide">
							<label for="password">Password:</label>
							<input type="password" class="input-text" name="password" value="">
						</p>

						<p class="form-row form-row-wide">
							<label for="password_confirmation">Repeat Password:</label>
							<input type="password" class="input-text" name="password_confirmation" value="">
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
						</p>
									
						<p class="form-row">
							<input type="submit" class="button" name="register" value="Register" />
						</p>
					</form>
			</div>

			<!-- Login -->
			<div class="tab-content" id="tab2" style="display: none;">

				<h3 class="margin-bottom-10 margin-top-10">Login </h3>
				<hr>
				<form method="post" action="{{URL::to('/')}}/auth/login" class="login">
					@if (count($errors) > 0)
							<div class="alert alert-danger" role="alert">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
							</div>
						@endif
		
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<p class="form-row form-row-wide">
						<label for="username">Username or Email Address:</label>
						<input type="email" class="input-text" name="email" value="">
					</p>

					<p class="form-row form-row-wide">
						<label for="password">Password:</label>
						<input type="password" class="input-text" name="password" value="">
					</p>

					<p class="form-row">
						<input type="submit" class="button" name="login" value="Login" />

						<label for="rememberme" class="rememberme">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> Remember Me</label>
					</p>

					<p class="lost_password">
						<a href="#" >Lost Your Password?</a>
					</p>
					
				</form>
			</div>
		</div>
	</div>


		<div class="margin-bottom-55"></div>
	</div>
	</div>

	<!-- Job Spotlight -->
	<div class="five columns" style="padding-top:45px;">
		<div class="clearfix"></div>
		
		<!-- Showbiz Container -->
		<div id="job-spotlight" class="showbiz-container">
			<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
				<div class="overflowholder">

					<ul>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Step 2 of 2 </h4></a>
								<p>Please Login to Post your requirement and complete your listing</p>
								<a onclick="winback()" class="button">Return to edit</a>
								<script type="text/javascript">
									function winback(){
										window.history.back();
									}
								</script>
							</div>
						</li>
					</ul>
					<div class="clearfix"></div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
@endsection