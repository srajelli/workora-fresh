@extends('app')
@section('content')
<!-- Counters -->
<div id="counters">
	<div class="container">

		<div class="four columns">
			<div class="counter-box">
				<span class="counter">150</span><i>+</i>
			</div>
		</div>

		<div class="eight columns">
			<div class="counter-box" style="text-align: left;border-right: none;">
				<p>Candidates For <?php echo $_GET['job'];?> Found in <?php echo $_GET['location'];?></p>
			</div>
		</div>		
	</div>
</div>

<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
<!-- Content
================================================== -->
<div class="">
	
	<!-- Submit Page -->
	<div class="">
		<div class="submit-page">
		<form action="{{URL::to('/')}}/users/jobs/post" method="POST">
			<!-- Notice -->
			<div class="notification notice closeable margin-bottom-40">
				<p><span>Note : </span> Please fill out the job details below to continue</p>
			</div>


			<!-- Title -->
			<div class="form">
				<h5>Job Title</h5>
				<input name="job-title" id="job-title" class="search-field" type="text" placeholder="" value=""/>
			</div>

			<!-- Location -->
			<div class="form">
				<h5>Location <span>(selected)</span></h5>
				<input name="location" name="location" class="search-field" type="text" value="<?php echo $_GET['location']?>" disabled/>
					
			</div>


			<!-- Choose Category -->
			<div class="form">
				<div class="select">
					<h5>Category <span>(selected)</span></h5>
					<input name="category" name="category" class="search-field" type="text"  value="<?php echo $_GET['job']?>" disabled/>					
				</div>
			</div>
			<div class="select">
			<h5>Select Experience: </h5>
			<select name="exp" id="exp" data-placeholder="Select Experience Level" class="chosen-select">
				<option value="1">Basic</option>
				<option value="2">Intermediate</option>
				<option value="3">Skilled</option>
				<option value="4">Experienced</option>
				<option value="5">Professional</option>
			</select>
			</div>

			<!-- Tags -->
			<div class="form">
				<br>
				<h5>Budget </h5>
				<input name="job-budget" name="job-budget" class="search-field" type="text" placeholder="₹ 10000" value=""/>
			</div>


			<!-- Description -->
			<div class="form">
				<h5>Description</h5>
				<textarea class="" name="job-desc" cols="20" rows="3" id="job-desc" spellcheck="true"></textarea>
			</div>


			<!-- TClosing Date -->
			<div class="form">
				<h5>Closing Date <span>(optional)</span></h5>
				<input name="deadline" name="deadline" data-role="date" type="text" placeholder="yyyy-mm-dd">
				<p class="note">Deadline for new applicants.</p>
			</div>

<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
			<div class="divider margin-top-0"></div>
			<button type="submit" class="button big margin-top-5">Submit <i class="fa fa-arrow-circle-right"></i></button>
		</form>

		</div>
	</div>

</div>
		<div class="margin-bottom-55"></div>
	</div>
	</div>

	<!-- Job Spotlight -->
	<div class="five columns" style="padding-top:45px;">
		<div class="clearfix"></div>
		
		<!-- Showbiz Container -->
		<div id="job-spotlight" class="showbiz-container">
			<div class="showbiz" data-left="#showbiz_left_1" data-right="#showbiz_right_1" data-play="#showbiz_play_1" >
				<div class="overflowholder">

					<ul>

						<li>
							<div class="job-spotlight">
								<a href="#"><h4>Step 1 of 2 </h4></a>
								<p>Fill up the following form to proceed further.</p>
								<a href="{{URL::to('/')}}/search/2" class="button">Submit</a>
							</div>
						</li>
					</ul>
					<div class="clearfix"></div>

				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
@endsection